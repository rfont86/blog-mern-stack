import React, { useState, useEffect } from 'react'
import { useParams } from "react-router-dom"
import axios from 'axios'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'

const CreateNote = () => {

  let { id } = useParams()

  const [paramId, setParamId] = useState(false)
  const [users, setUsers] = useState([])
  const [selected, setSelected] = useState("")
  const [title, setTitle] = useState("")
  const [content, setContent] = useState("")
  const [date, setDate] = useState(new Date())

  const fetchData = async () => {
    const result = await axios.get('http://localhost:4000/api/users')
    setUsers(result.data) 
    setSelected(result.data[0].username)
    
    if(id){
      const response = await axios.get('http://localhost:4000/api/notes/' + id)
      setSelected(response.data.author)
      setTitle(response.data.title)
      setContent(response.data.content)
      setDate(new Date(response.data.date))
      setParamId(true)
    }
  }

  useEffect(() => {
    fetchData()
  }, [])
  
  const onSubmit = async event => {
    event.preventDefault()
    const newNote = {
      title: title,
      content: content,
      author: selected,
      date: date
    }
    if(paramId){
      await axios.put('http://localhost:4000/api/notes/' + id, newNote)
    }else{
      await axios.post('http://localhost:4000/api/notes', newNote)
    }
    window.location.href = '/'
  }

  const onInputUser = event => {
    setSelected(event.target.value)
  }

  const onInputTitle = event => {
    setTitle(event.target.value)
  }

  const onInputContent = event => {
    setContent(event.target.value)
  }

  const onChangeDate = date => {
    setDate(date)
  }

  return (
    <div className="col-md-6 offset-md-3">
      <div className="card card-body">
        <h4>Create a Note</h4>

        <div className="form-group">
          <select 
            className="form-control"
            name="userSelected"
            value={selected}
            onChange={onInputUser}
          >
            {
              users.map(user => (
                <option key={user._id} value={user.username} >
                  {user.username}
                </option>
              ))
            }
          </select>
        </div>

        <div className="form-group">
          <input 
            type="text"
            className="form-control"
            placeholder="Title"
            name="title"
            value={title}
            onChange={onInputTitle}
            required
          />
        </div>

        <div className="form-group">
          <textarea
            name="content"
            className="form-control"
            placeholder="Content"
            value={content}
            onChange={onInputContent}
            required
          />
        </div>

        <div className="form-group">
          <DatePicker 
            className="form-control"
            selected={date}
            onChange={onChangeDate}
          />
        </div>

        <form onSubmit={onSubmit} >
          <button type="submit" className="btn btn-primary">
            Save
          </button>
        </form>
      </div>
    </div>
  )
}

export default CreateNote