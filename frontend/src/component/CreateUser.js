import React, { useState, useEffect } from 'react'
import axios from 'axios'

const CreateUser = () => {

  const [users, setUsers] = useState([])
  const [aUser, setUsername] = useState()
  console.log(users)

  const fetchData = async () => {
    const result = await axios.get('http://localhost:4000/api/users')
    setUsers(result.data) 
  }

  useEffect(() => {
    fetchData()
  }, [])

  const onChangeUsername = (event) => {
    setUsername(event.target.value)
  }

  const onSubmit = async (event) => {
    event.preventDefault()
    await axios.post('http://localhost:4000/api/users', {
      username: aUser
    })
    setUsername('')
    fetchData()
  }

  const deleteUser = async (id) => {
    await axios.delete(`http://localhost:4000/api/users/${id}`)
    fetchData()
  }

  return (
    <div className="row">
      <div className="col-md-4">
        <div className="card card-body">
          <h3>Create New User</h3>
          <form onSubmit={onSubmit}>
            <div className="form-group">
              <input 
                type="text" 
                className="from-control" 
                value={aUser} 
                onChange={onChangeUsername} 
              />
            </div>
            <button type="submit" className="btn btn-primary">
              Save
            </button>
          </form>
        </div>
      </div>
      <div className="col-md-8">
        <ul className="list-group">
          {
            users.map(user => (
              <li 
                className="list-group-item list-group-item-action" 
                key={user._id} 
                onDoubleClick={() => deleteUser(user._id)}
              >
                {user.username}
              </li>
            ))
          }
        </ul>
      </div>
    </div>

  )
}

export default CreateUser