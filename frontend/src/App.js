import React from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';

import Navigation from './component/Navigation';
import CreateNote from './component/CreateNote';
import CreateUser from './component/CreateUser';
import NoteList from './component/NotesList';

export const App = () => (
    <>
      <Router>
        <Navigation />
        <div className="container p-4">
          <Route path="/" component={NoteList} exact />
          <Route path="/edit/:id" component={CreateNote} />
          <Route path="/create" component={CreateNote} />
          <Route path="/user" component={CreateUser} />
        </div>
      </Router>
    </>
  )

export default App