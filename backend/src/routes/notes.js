const {Router} = require('express')
const router = Router()

const { getNotes, createNote, getNote, deletNote, updateNote } = require('../controllers/notes.controller')

router.route('/')
  .get(getNotes)
  .post(createNote)

router.route('/:id')
  .delete(deletNote)
  .put(updateNote)
  .get(getNote)

module.exports = router