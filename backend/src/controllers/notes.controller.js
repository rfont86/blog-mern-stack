const notesController = {}

const Note = require('../models/Note')

notesController.getNotes = async (request, response) => {
  const notes = await Note.find()
  response.json(notes)
}

notesController.createNote = async (request, response) => {
  const {title, content, date, author} = request.body
  const newNote = new Note({
    title,
    content,
    date,
    author
  })
  await newNote.save()
  response.json({message: 'Nota guardada'})
}

notesController.getNote = async (request, response) => {
  const note = await Note.findById(request.params.id)
  response.json(note)
}

notesController.updateNote = async (request, response) => {
  const {title, content, author} = request.body
  await Note.findOneAndUpdate(request.params.id, {
    title,
    content,
    author
  })
  response.json({message: 'update note'})
}

notesController.deletNote = async (request, response) => {
  await Note.findByIdAndDelete(request.params.id)
  response.json({message: 'Nota eliminada'})
}
module.exports = notesController