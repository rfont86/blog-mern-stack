const userCotroller = {}

const User = require('../models/User')

userCotroller.getUsers = async (request, response) => {
  const users = await User.find()
  response.json(users)
}

userCotroller.createUser = async (request, response) => {
  const { username } = request.body
  const newUser = new User({username})
  await newUser.save()
  response.json("usuario guardado")
}

userCotroller.deleteUser = async (request, response) => {
  await User.findByIdAndDelete(request.params.id)
  response.json('user deleted')
}

module.exports = userCotroller